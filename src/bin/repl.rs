use concrete::parser::Parse;
use std::io;
use std::io::Write;

fn main() {
    println!("Concrete REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .ok()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                let mut parse = Parse::new(&input, concrete::root::NAMESPACE);
                let parsed_type = parse.execute();
                println!("ECHO: {}", parsed_type);
                println!("EVAL: {}", parsed_type.evaluate());
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
