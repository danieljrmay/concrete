use concrete::parser::Tokenize;
use std::io;
use std::io::Write;

fn main() {
    println!("Concrete Token REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .ok()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                print!(" IN: {}", &input);

                let mut tokenize = Tokenize::new(&input);
                let token_list = tokenize.execute();
                println!("OUT: {}", token_list);
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
