use crate::mathml::markup::Markup;
use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "mathml/SerializeAsParallelMarkup";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::Type(t) = args.remove(0) {
            return Ok(Type::MathMLSerializeAsParallelMarkup(Box::new(t)));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'Type' argument. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(arg: &Box<Type>) -> Type {
    let mut m = Markup::new();
    m.append_math_start();
    m.append_default_namespace();
    m.append_stag_stop();

    m.append_semantics_start();
    m.append_stag_stop();
    crate::mathml::serialize_as_presentation_markup::serialize_as_presentation(&mut m, arg);

    m.append_annotation_xml_encoding_mathml_content_start();
    m.append_stag_stop();
    crate::mathml::serialize_as_content_markup::serialize_as_content(&mut m, arg);
    m.append_annotation_xml_stop();

    m.append_semantics_stop();

    m.append_math_stop();

    Type::MathMLMarkup(m)
}
