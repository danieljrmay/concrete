use crate::mathml::markup::Markup;
use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "mathml/SerializeAsContentMarkup";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::Type(t) = args.remove(0) {
            return Ok(Type::MathMLSerializeAsContentMarkup(Box::new(t)));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'Type' argument. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(arg: &Box<Type>) -> Type {
    let mut m = Markup::new();
    m.append_math_start();
    m.append_default_namespace();
    m.append_stag_stop();

    serialize_as_content(&mut m, arg);

    m.append_math_stop();

    Type::MathMLMarkup(m)
}

pub fn serialize_as_content(m: &mut Markup, t: &Type) {
    match t {
        Type::And(args) => {
            m.append_apply_start();
            m.append_stag_stop();

            m.append_and_start();
            m.append_empty_tag_stop();

            for arg in args {
                serialize_as_content(m, arg);
            }

            m.append_apply_stop();
        }
        Type::Boolean(rb) => {
            if *rb {
                m.append_true_start();
                m.append_empty_tag_stop();
            } else {
                m.append_false_start();
                m.append_empty_tag_stop();
            }
        }
        Type::ErrorMessage(text) => {
            m.append_merror_start();
            m.append_stag_stop();
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
            m.append_merror_stop();
        }
        Type::False => {
            m.append_false_start();
            m.append_empty_tag_stop();
        }
        Type::Name(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::Namespace(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::Not(arg) => {
            m.append_apply_start();
            m.append_stag_stop();

            m.append_not_start();
            m.append_empty_tag_stop();

            serialize_as_content(m, arg);
            m.append_apply_stop();
        }
        Type::QualifiedName(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::True => {
            m.append_true_start();
            m.append_empty_tag_stop();
        }
        Type::MathMLMarkup(markup) => m.append_mathml_markup(markup),
        Type::MathMLSerializeAsContentMarkup(arg) => serialize_as_content(m, arg),
        Type::MathMLSerializeAsParallelMarkup(_) => (), // TODO
        Type::MathMLSerializeAsPresentationMarkup(_) => (), // TODO: Not sure what to do about this type here
    }
}
