use crate::mathml::markup::Markup;
use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "mathml/SerializeAsPresentationMarkup";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::Type(t) = args.remove(0) {
            return Ok(Type::MathMLSerializeAsPresentationMarkup(Box::new(t)));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'Type' argument. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(arg: &Box<Type>) -> Type {
    let mut m = Markup::new();
    m.append_math_start();
    m.append_default_namespace();
    m.append_stag_stop();

    serialize_as_presentation(&mut m, arg);

    m.append_math_stop();

    Type::MathMLMarkup(m)
}

pub fn serialize_as_presentation(m: &mut Markup, t: &Type) {
    match t {
        Type::And(args) => {
            m.append_mrow_start();
            m.append_stag_stop();

            for arg in &args[..args.len() - 1] {
                serialize_as_presentation(m, arg);
                m.append_mo_start();
                m.append_stag_stop();
                m.append_boolean_and();
                m.append_mo_stop();
            }

            serialize_as_presentation(m, &args[args.len() - 1]);

            m.append_mrow_stop();
        }
        Type::Boolean(rb) => {
            if *rb {
                m.append_mi_start();
                m.append_stag_stop();
                m.append_boolean_one();
                m.append_mi_stop();
            } else {
                m.append_mi_start();
                m.append_stag_stop();
                m.append_boolean_zero();
                m.append_mi_stop();
            }
        }
        Type::ErrorMessage(text) => {
            m.append_merror_start();
            m.append_stag_stop();
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
            m.append_merror_stop();
        }
        Type::False => {
            m.append_mi_start();
            m.append_stag_stop();
            m.append_boolean_zero();
            m.append_mi_stop();
        }
        Type::Name(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::Namespace(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::Not(arg) => {
            m.append_mrow_start();
            m.append_stag_stop();

            m.append_mo_start();
            m.append_stag_stop();
            m.append_boolean_not();
            m.append_mo_stop();

            serialize_as_presentation(m, arg);

            m.append_mrow_stop();
        }
        Type::QualifiedName(text) => {
            m.append_mtext_start();
            m.append_stag_stop();
            m.append_text(text);
            m.append_mtext_stop();
        }
        Type::True => {
            m.append_mi_start();
            m.append_stag_stop();
            m.append_boolean_one();
            m.append_mi_stop();
        }
        Type::MathMLMarkup(markup) => m.append_mathml_markup(markup),
        Type::MathMLSerializeAsContentMarkup(_) => (), // TODO: Not sure what to do about this type here
        Type::MathMLSerializeAsParallelMarkup(_) => (), // TODO
        Type::MathMLSerializeAsPresentationMarkup(arg) => serialize_as_presentation(m, arg),
    }
}
