use crate::Argument;
use crate::Parameters;
use crate::Type;
use std::convert::From;

pub const QNAME: &'static str = "mathml/Markup";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::String(s) = args.remove(0) {
            return Ok(Type::MathMLMarkup(Markup::from(s)));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'String' argument. Got: {:?}.",
        QNAME, args
    )))
}

// MathML Namespace
const NAMESPACE_URI: &'static str = "http://www.w3.org/1998/Math/MathML";

// MathML Element Tags
const AND: &'static str = "and";
const ANNOTATION_XML: &'static str = "annotation-xml";
const APPLY: &'static str = "apply";
const MATH: &'static str = "math";
const MERROR: &'static str = "merror";
const MI: &'static str = "mi";
const MO: &'static str = "mo";
const MROW: &'static str = "mrow";
const MTEXT: &'static str = "mtext";
const NOT: &'static str = "not";
const SEMANTICS: &'static str = "semantics";
const TRUE: &'static str = "true";
const FALSE: &'static str = "false";

// MathML Attribute Namespace
const ENCODING: &'static str = "encoding";

// MathML Attribute Values
const MATHML_CONTENT: &'static str = "MathML-Content";

#[derive(Clone, Debug)]
pub struct Markup {
    xml: crate::xml::markup::Markup,
}
impl Markup {
    pub fn new() -> Markup {
        Markup {
            xml: crate::xml::markup::Markup::new(),
        }
    }

    pub fn xml_markup(&self) -> &crate::xml::markup::Markup {
        &self.xml
    }

    pub fn parameters(&self) -> Parameters {
        self.xml.parameters()
    }

    pub fn append_default_namespace(&mut self) {
        self.xml.append_default_namespace(NAMESPACE_URI)
    }

    pub fn append_and_start(&mut self) {
        self.xml.append_empty_tag_start(AND);
    }

    pub fn append_annotation_xml_start(&mut self) {
        self.xml.append_stag_start(ANNOTATION_XML);
    }

    pub fn append_annotation_xml_encoding_mathml_content_start(&mut self) {
        self.xml.append_stag_start(ANNOTATION_XML);
        self.xml.append_attribute(ENCODING, MATHML_CONTENT);
    }

    pub fn append_annotation_xml_stop(&mut self) {
        self.xml.append_etag(ANNOTATION_XML);
    }

    pub fn append_apply_start(&mut self) {
        self.xml.append_stag_start(APPLY);
    }

    pub fn append_apply_stop(&mut self) {
        self.xml.append_etag(APPLY);
    }

    pub fn append_empty_tag_stop(&mut self) {
        self.xml.append_empty_tag_stop();
    }

    pub fn append_math_start(&mut self) {
        self.xml.append_stag_start(MATH);
    }

    pub fn append_math_stop(&mut self) {
        self.xml.append_etag(MATH);
    }

    pub fn append_merror_start(&mut self) {
        self.xml.append_stag_start(MERROR);
    }

    pub fn append_merror_stop(&mut self) {
        self.xml.append_etag(MERROR);
    }

    pub fn append_mi_start(&mut self) {
        self.xml.append_stag_start(MI);
    }

    pub fn append_mi_stop(&mut self) {
        self.xml.append_etag(MI);
    }

    pub fn append_mo_start(&mut self) {
        self.xml.append_stag_start(MO);
    }

    pub fn append_mrow_stop(&mut self) {
        self.xml.append_etag(MROW);
    }

    pub fn append_mrow_start(&mut self) {
        self.xml.append_stag_start(MROW);
    }

    pub fn append_mo_stop(&mut self) {
        self.xml.append_etag(MO);
    }

    pub fn append_mtext_start(&mut self) {
        self.xml.append_stag_start(MTEXT);
    }

    pub fn append_mtext_stop(&mut self) {
        self.xml.append_etag(MTEXT);
    }

    pub fn append_false_start(&mut self) {
        self.xml.append_empty_tag_start(FALSE);
    }

    pub fn append_not_start(&mut self) {
        self.xml.append_empty_tag_start(NOT);
    }

    pub fn append_semantics_start(&mut self) {
        self.xml.append_stag_start(SEMANTICS);
    }

    pub fn append_semantics_stop(&mut self) {
        self.xml.append_etag(SEMANTICS);
    }

    pub fn append_stag_stop(&mut self) {
        self.xml.append_stag_stop();
    }

    pub fn append_true_start(&mut self) {
        self.xml.append_empty_tag_start(TRUE);
    }

    pub fn append_text(&mut self, text: &str) {
        self.xml.append_text(text);
    }

    pub fn append_mathml_markup(&mut self, markup: &Markup) {
        self.xml.append_xml_markup(markup.xml_markup());
    }

    pub fn append_boolean_zero(&mut self) {
        self.xml.append_text("𝟢");
    }

    pub fn append_boolean_one(&mut self) {
        self.xml.append_text("𝟣");
    }

    pub fn append_boolean_and(&mut self) {
        self.xml.append_text("∧");
    }

    pub fn append_boolean_not(&mut self) {
        self.xml.append_text("¬");
    }
}
impl From<String> for Markup {
    fn from(s: String) -> Self {
        Markup {
            xml: crate::xml::markup::Markup::from(s),
        }
    }
}
