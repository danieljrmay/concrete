pub mod markup;
pub mod serialize_as_content_markup;
pub mod serialize_as_parallel_markup;
pub mod serialize_as_presentation_markup;

pub const NAMESPACE: &'static str = "mathml/";
