use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "/Not";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::Type(t) = args.remove(0) {
            return Ok(Type::Not(Box::new(t)));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'Type' argument. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(arg: &Box<Type>) -> Type {
    let earg = arg.evaluate();

    match earg {
        Type::False => Type::True,
        Type::True => Type::False,
        _ => Type::Not(Box::new(earg)),
    }
}
