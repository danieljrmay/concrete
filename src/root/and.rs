use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "/And";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() >= 2 {
        let mut vt: Vec<Type> = Vec::new();

        while args.len() > 0 {
            match args.remove(0) {
                Argument::Type(t) => vt.push(t),
                _ => panic!("Expected only Argument::Type"),
            }
        }

        return Ok(Type::And(vt));
    }

    Err(Type::ErrorMessage(format!(
        "{} expects >= 2 'Type' arguments. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(args: &Vec<Type>) -> Type {
    let mut result_args: Vec<Type> = Vec::new();

    for arg in args.iter() {
        let earg = arg.evaluate();

        match earg {
            Type::False => return Type::False,
            Type::True => (),
            _ => {
                result_args.push(earg);
            }
        }
    }

    if result_args.len() == 0 {
        return Type::True;
    } else {
        result_args.push(Type::True);

        return Type::And(result_args);
    }
}
