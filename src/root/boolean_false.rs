use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "/False";

pub fn new(args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 0 {
        return Ok(Type::False);
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 0 arguments. Got: {:?}.",
        QNAME, args
    )))
}
