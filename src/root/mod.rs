pub mod and;
pub mod boolean;
pub mod boolean_false;
pub mod boolean_true;
pub mod error_message;
pub mod name;
pub mod namespace;
pub mod not;
pub mod qualified_name;

pub const NAMESPACE: &'static str = "/";
