use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "/Boolean";

pub fn new(args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::Bool(b) = args[0] {
            return Ok(Type::Boolean(b));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'Bool' argument. Got: {:?}.",
        QNAME, args
    )))
}

pub fn evaluate(b: &bool) -> Type {
    if *b {
        return Type::True;
    } else {
        return Type::False;
    }
}
