use crate::Argument;
use crate::Type;

pub const QNAME: &'static str = "/Name";

pub fn new(mut args: Vec<Argument>) -> Result<Type, Type> {
    if args.len() == 1 {
        if let Argument::String(s) = args.remove(0) {
            return Ok(Type::Name(s));
        }
    }

    Err(Type::ErrorMessage(format!(
        "{} expects 1 'String' argument. Got: {:?}.",
        QNAME, args
    )))
}
