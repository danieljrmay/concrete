use crate::Parameters;
use std::convert::From;

const XMLNS: &'static str = "xmlns";

#[derive(Clone, Debug)]
pub struct Markup {
    s: String,
}
impl Markup {
    pub fn new() -> Markup {
        Markup { s: String::new() }
    }

    pub fn as_str(&self) -> &str {
        &self.s
    }

    pub fn parameters(&self) -> Parameters {
        Parameters::RefStr(&self.s)
    }

    pub fn append_default_namespace(&mut self, namespace_uri: &str) {
        self.s
            .push_str(&format!(" {}=\"{}\"", XMLNS, namespace_uri));
    }

    pub fn append_prefixed_namespace(&mut self, prefix: &str, namespace_uri: &str) {
        self.s
            .push_str(&format!(" {}:{}=\"{}\"", XMLNS, prefix, namespace_uri));
    }

    pub fn append_attribute(&mut self, qname: &str, value: &str) {
        self.s.push_str(&format!(" {}=\"{}\"", qname, value));
    }

    pub fn append_prefixed_attribute(&mut self, prefix: &str, local_name: &str, value: &str) {
        self.s
            .push_str(&format!(" {}:{}=\"{}\"", prefix, local_name, value));
    }

    pub fn append_comment(&mut self, comment: &str) {
        self.s.push_str(&format!("<!--{}-->", comment));
    }

    pub fn append_processing_instruction(&mut self, target: &str, data: &str) {
        if data.is_empty() {
            self.s.push_str(&format!("<?{}?>", target));
        } else {
            self.s.push_str(&format!("<?{} {}?>", target, data));
        }
    }

    pub fn append_cdata(&mut self, cdata: &str) {
        self.s.push_str(&format!("<![CDATA[{}]]>", cdata));
    }

    pub fn append_stag_start(&mut self, tag: &str) {
        self.s.push_str(&format!("<{}", tag));
    }

    pub fn append_stag_stop(&mut self) {
        self.s.push_str(">");
    }

    pub fn append_etag(&mut self, tag: &str) {
        self.s.push_str(&format!("</{}>", tag));
    }

    pub fn append_empty_tag_start(&mut self, tag: &str) {
        self.s.push_str(&format!("<{}", tag));
    }

    pub fn append_empty_tag_stop(&mut self) {
        self.s.push_str("/>");
    }

    pub fn append_text(&mut self, text: &str) {
        self.s.push_str(text);
    }

    pub fn append_xml_markup(&mut self, markup: &Markup) {
        self.s.push_str(markup.as_str())
    }
}
impl From<String> for Markup {
    fn from(s: String) -> Self {
        Markup { s }
    }
}
// Note all these functions are UNCHECKED. It is assumed that they are
// being used safley e.g. check comment body for instances of "-->",
// etc.
